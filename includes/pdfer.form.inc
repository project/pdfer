<?php
/**
 * @file
 * Form alters and other operations with forms.
 */

/**
 * Form adding or editing pdf template.
 */
function pdfer_add_edit_template_form($form, &$form_state, $template_name = NULL) {
  if (!empty($template_name)) {
    $result = pdfer_load_existing_template_by_template_id($template_name);
    $disabled = array('#disabled' => TRUE);
  }

  $form['add_template'] = array(
    '#title' => t('Add pdf template'),
    '#type' => 'fieldset',
  );

  $form['add_template']['name'] = array(
    '#title' => t('Template Name'),
    '#type' => 'textfield',
    '#default_value' => isset($result->name) ? $result->name : '',
    '#description' => t('Unique name of your template'),
    '#required' => TRUE,
  );
  if (isset($disabled)) {
    $form['add_template']['name'] += $disabled;
  }

  $languages = language_list();
  foreach ($languages as $language) {
    $select_languages[$language->name] = $language->name;
  }

  $form['add_template']['language'] = array(
    '#title' => t('Template language'),
    '#type' => 'select',
    '#options' => $select_languages,
    '#default_value' => isset($result->language) ? $result->language : '',
    '#description' => t('Language of your template'),
  );

  $form['add_template']['description'] = array(
    '#title' => t('Template description'),
    '#type' => 'textfield',
    '#description' => t('Description of your template'),
    '#default_value' => isset($result->description) ? $result->description : '',
  );

  $form['add_template']['template'] = array(
    '#title' => t('PDF Template'),
    '#type' => 'text_format',
    '#default_value' => isset($result->template) ? htmlspecialchars_decode($result->template) : '',
    '#required' => TRUE,
    '#wysiwyg' => TRUE,
  );

  $form['add_template']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit for pdfer_add_edit_template_form.
 */
function pdfer_add_edit_template_form_submit($form, &$form_state) {
  $fields = array('name', 'language', 'description', 'template');
  $template_data = array();
  foreach ($fields as $field) {
    if (isset($form_state['values'][$field])) {
      if ($field == 'template') {
        $template_data[$field] = htmlspecialchars_decode($form_state['values']['template']['value']);
      }
      else {
        $template_data[$field] = $form_state['values'][$field];
      }
    }
  }
  if (!empty($template_data)) {
    db_merge('pdfer')
      ->key(array('name' => $template_data['name']))
      ->fields($template_data)
      ->execute();
    drupal_set_message(t('Your changes have been saved successfully.'));
    $form_state['redirect'] = array(
      PDFER_DEFAULT_SETTINGS_PATH,
      array(),
      302,
    );
  }
}
