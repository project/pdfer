<?php
/**
 * @file
 * Administrative interface for the PDFer module.
 */

/**
 * Generates main table with all templates.
 */
function pdfer_admin_page() {
  $header = array(
    t('Template ID'),
    t('Template Name'),
    t('Language'),
    t('Description'),
    t('Operations'),
  );
  $rows = array();

  $text = t('Add a template');
  $url = PDFER_DEFAULT_SETTINGS_PATH . '/add-template';
  $link = array('title' => $text, 'href' => $url);
  $variables = array(
    'links' => array($link),
    'attributes' => array('class' => array('action-links')),
  );
  $output = theme('links', $variables);

  $results = db_select('pdfer', 'pdf')
    ->fields('pdf')
    ->execute()
    ->fetchAll();
  if (!empty($results)) {
    foreach ($results as $result) {
      $edit_link = l(t('Edit'), PDFER_DEFAULT_SETTINGS_PATH . '/' . $result->id . '/edit');
      $download_link = l(t('Test download'), PDFER_DEFAULT_SETTINGS_PATH . '/' . $result->id . '/test-download');
      $review_as_html = l(t('Review as HTML'), PDFER_DEFAULT_SETTINGS_PATH . '/' . $result->id . '/review-as-html');
      $operations = $edit_link . '|' . $download_link . '|' . $review_as_html;

      $rows[] = array(
        check_plain($result->id),
        check_plain($result->name),
        check_plain($result->language),
        check_plain($result->description),
        $operations,
      );
    }
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $output .= t('Please add one or more templates.');
  }
  return $output;
}

/**
 * Function for test pdf downloading.
 */
function pdfer_test_download($template_id) {
  pdfer_wkhtmltopdf($template_id);
}

/**
 * Function for reviewing HTML on page.
 */
function pdfer_review_as_html($template_id) {
  $pdf_template = pdfer_load_existing_template_by_template_id($template_id);
  if (module_exists('token')) {
    $pdf_template->template = token_replace($pdf_template->template);
  }

  return $pdf_template->template;
}
