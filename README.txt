INTRODUCTION
------------

The PDFer module provides adding, saving, editing html-templates or simple data 
and then downloading it like PDF file.  

 * For a full description of the module, visit the project page:
   

 * To submit bug reports and feature suggestions, or to track changes:
  

REQUIREMENTS
------------
This module requires the following modules:

 * PHP WK HTML to PDF (https://www.drupal.org/project/phpwkhtmltopdf)

RECOMMENDED MODULES
-------------------
 * Wysiwyg (https://www.drupal.org/project/wysiwyg):
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer pdf templates

     Allows user to add, edit pdf-templates and settings.

TROUBLESHOOTING
---------------

FAQ
---

Q: I added new template, checked if everything was downloaded correctly
with "Test download" button but how I can use this functionality in my modules?

A: You can use main function for generating "pdfer_wkhtmltopdf"
passing there template id, template itself and path where you want to save pdf.

MAINTAINERS
-----------

Current maintainers:
 * Andrey Troeglazov (andrey.troeglazov) - https://drupal.org/user/3145389
